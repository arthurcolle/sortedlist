#include <stdio.h>
#include <stdlib.h>
#include "list.h"

void init_list(List **list) {
  if (*list == NULL) { *list = malloc(sizeof(List)); }
  else { printf("List already initialized!\n"); }
}

void print(List *list) {
  Node *temp = list->head;
  while (temp->next != NULL) {
    printf("%d\n", temp->data);
    temp = temp->next;
  }
  printf("%d\n", temp->data);
}

List *insert_sort(List **list, int num) {
  if ((*list) == NULL) /* same as (*list) == NULL */
    init_list(list);
    
    
  if ((*list)->head == NULL) { /* same as (*list)->next == NULL */
    (*list)->head = malloc(sizeof(Node));
    (*list)->head->data = num;
    (*list)->head->next = NULL;
  }
  else {
    Node *new_node = NULL;
    if ((*list)->head->data > num) {
      new_node = malloc(sizeof(Node));
      new_node->next = (*list)->head;
      new_node->data = num;
      (*list)->head = new_node;
    }
    else {
      new_node = (*list)->head;
      Node *back = NULL;
      while (num > new_node->data && new_node->next != NULL) {
        back = new_node;
        new_node = new_node->next;
      }
      if (new_node->next != NULL) {
        back->next = malloc(sizeof(Node));
        back->next->data = num;
        back->next->next = new_node;
      }
      else {
        new_node->next = malloc(sizeof(Node));
        new_node->next->data = num;
        new_node->next->next = NULL;
      }
    }
  }
  return *list;
}

List *head_add(List **list, int num) {
  Node *temp = (*list)->head;
  (*list)->head = malloc(sizeof(Node));
  (*list)->head->data = num;
  (*list)->head->next = temp;
  return *list;
}

List *tail_add(List **list, int num) {
/*  if (*list == NULL)
    init_list(list);

  if ((*list)->head == NULL) {
    (*list)->head = malloc(sizeof(Node));
    (*list)->head->data = num;
    (*list)->head->next = NULL;
  }
  else {
    Node *temp = (*list)->head;
    while (temp->next != NULL)
      temp = temp->next;
    temp->next = malloc(sizeof(Node));
    (*list)->tail = temp->next;
    temp->next->data = num;
    temp->next->next = NULL;
  }
  return *list;*/
}


