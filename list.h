typedef struct node {
  struct node *next;
  int data;
} Node;

typedef struct {
  Node *head;
  Node *tail;
} List;

void print(List*);
List *tail_add(List**, int);
List *head_add(List**, int);
List *insertion_sort(List**, int);
