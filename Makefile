all: main.x

main.x: driver.o list.o
	gcc driver.o list.o -o main.x

driver.o: driver.c
	gcc -c driver.c
  
list.o: list.c list.h
	gcc -c list.c
	
clean:
	rm *.o *.x
