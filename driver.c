#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "list.h"

int main(void) {
  srand(time(NULL));
  List *list = NULL;
  int i = 10;
  while (i > -10) {
    insertion_sort(&list, rand()%50);
    i-=1;
  }
  print(list);
  return 0;
}
